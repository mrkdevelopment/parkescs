import {Injectable, Pipe} from '@angular/core';
import * as moment from 'moment';

/*
  Generated class for the DateDisplay pipe.

  See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
  Angular 2 Pipes.
*/
@Pipe({
  name: 'DateDisplay'
})
@Injectable()
export class DateDisplay {
  /*
    Takes a value and makes it lowercase.
   */
  transform(value: string, args: any[]) {
    value = value + ''; // make sure it's a string
    return moment(value).format('MMMM Do YYYY');
  }
}
