import {Page, NavController} from 'ionic-angular';

/*
  Generated class for the ContactPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/contact/contact.html',
})
export class ContactPage {
  constructor(public nav: NavController) {}
}
