import {Page, NavController} from 'ionic-angular';
import {WpFaq} from '../../providers/wp-faq/wp-faq';
import {DateDisplay} from '../../pipes/date-display';

/*
  Generated class for the FaqPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/faq/faq.html',
  providers: [WpFaq],
  pipes: [DateDisplay]
})
export class FaqPage {
  
  public items;

  constructor(public nav: NavController, public myWpFaq: WpFaq) {
	  this.myWpFaq.load().then((data) => {
		  this.items = data;
	  });
  }
}
