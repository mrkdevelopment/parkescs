import {Page, NavController} from 'ionic-angular';
import {WpEvents} from '../../providers/wp-events/wp-events';
import {DateDisplay} from '../../pipes/date-display';

/*
  Generated class for the EventsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/events/events.html',
  providers: [WpEvents],
  pipes: [DateDisplay]
})
export class EventsPage {

  public items;

  constructor(public nav: NavController, public myWpEvents: WpEvents) {
  	this.myWpEvents.load().then((data) => {
		this.items = data;
	});
  }
}
