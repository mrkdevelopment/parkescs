import {Page, NavController} from 'ionic-angular';
import {WpStaff} from '../../providers/wp-staff/wp-staff';
import {DateDisplay} from '../../pipes/date-display';

/*
  Generated class for the TeachersPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/teachers/teachers.html',
  providers: [WpStaff],
  pipes: [DateDisplay]
})
export class TeachersPage {

	public items;

	constructor(public nav: NavController, public myWpStaff: WpStaff) {
		this.myWpStaff.load().then((data) => {
			this.items = data;
		});
  	}
}
