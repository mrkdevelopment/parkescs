import {Page, NavController} from 'ionic-angular';
import {WpNews} from '../../providers/wp-news/wp-news';
import {DateDisplay} from '../../pipes/date-display';

/*
  Generated class for the NewsPage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Page({
  templateUrl: 'build/pages/news/news.html',
  providers: [WpNews],
  pipes: [DateDisplay]
})
export class NewsPage {
	
	public items;

	constructor(public nav: NavController, public myWpNews: WpNews) {
	  this.myWpNews.load().then((data) => {
		  this.items = data;
	  });
  }
}
